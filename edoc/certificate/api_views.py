from datetime import datetime

from django.db import IntegrityError
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from edoc.certificate.models import Certificate
from edoc.certificate.serializers import CertificateSerializer
from edoc.certificate.forms import CertificateForm
from edoc.utils.authenticate import TokenAuthentication
from edoc.eusign import EUSignCP


class CertificateListApi(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        eusign = EUSignCP.EUGetInterface()
        eusign.Initialize()

        cert_bytes = bytes()
        signer_info = {}

        form = CertificateForm(request.POST, request.FILES)
        if not form.is_valid():
            print(dict(form.errors))
            return Response({'errors': dict(form.errors)}, status=status.HTTP_400_BAD_REQUEST)

        sign = form.cleaned_data['sign'].read()

        try:
            eusign.GetSignerInfo(0, None, sign, len(sign), signer_info, cert_bytes)
        except RuntimeError:
            return Response({'errors': {
                'common': 'Загруженный файл не является подписью.'
            }}, status=status.HTTP_400_BAD_REQUEST)

        del form.cleaned_data['sign']

        print(signer_info)

        if signer_info['pszSerial'] != form.cleaned_data['serial']:
            return Response({'errors': {
                'common': 'Данные сертификата и подписи не совпадают.'
            }}, status=status.HTTP_400_BAD_REQUEST)

        new_cert = Certificate(
            issuer_index=form.cleaned_data['issuer_index'],
            organization=signer_info['pszSubjOrg'],
            name=signer_info['pszSubjFullName'],
            edrpou=signer_info['pszSubjEDRPOUCode'],
            drfo=signer_info['pszSubjDRFOCode'],
            serial=signer_info['pszSerial'],
            valid_to=datetime(
                signer_info['stPrivKeyEndTime']['wYear'],
                signer_info['stPrivKeyEndTime']['wMonth'],
                signer_info['stPrivKeyEndTime']['wDay'],
                signer_info['stPrivKeyEndTime']['wHour'],
                signer_info['stPrivKeyEndTime']['wMinute'],
                signer_info['stPrivKeyEndTime']['wSecond'],
                signer_info['stPrivKeyEndTime']['wMilliseconds']*1000
            ),
            title=signer_info['pszSubjTitle'],
            user=request.user
        )
        try:
            new_cert.save()
        except IntegrityError:
            return Response({'errors': {
                'common': 'Данный сертификат уже добавлен.'
            }}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'data': {'status': 'OK'}}, status=status.HTTP_201_CREATED)

    def get(self, request):
        certificates = Certificate.objects.filter(user=request.user)
        serializer = CertificateSerializer(certificates, many=True)
        return Response({'data': serializer.data}, status=status.HTTP_200_OK)


class CertificateDetailApi(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, serial):
        certificate = Certificate.objects.get(serial=serial, user=request.user)
        serializer = CertificateSerializer(certificate)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, serial):
        Certificate.objects.filter(serial=serial, user=request.user).delete()
        return Response({'data': {'status': 'OK'}}, status=status.HTTP_200_OK)
