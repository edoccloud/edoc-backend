from django.urls import path
from edoc.certificate.api_views import CertificateListApi
from edoc.certificate.api_views import CertificateDetailApi

urlpatterns = [
    path('', CertificateListApi.as_view()),
    path('<str:serial>', CertificateDetailApi.as_view()),
]