from django import forms


class CertificateForm(forms.Form):
    issuer_index = forms.IntegerField()
    organization = forms.CharField(required=False)
    name = forms.CharField()
    edrpou = forms.CharField(required=False)
    drfo = forms.CharField(required=False)
    serial = forms.CharField()
    sign = forms.FileField()
    title = forms.CharField(required=False)
