# Generated by Django 2.1 on 2018-09-05 14:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Certificate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('issuer_index', models.IntegerField(default=0, verbose_name='Индекс АЦСК')),
                ('organization', models.CharField(blank=True, max_length=100, verbose_name='Название организации')),
                ('name', models.CharField(blank=True, max_length=255, verbose_name='ФИО')),
                ('edrpou', models.CharField(blank=True, max_length=11, verbose_name='ЕДРПОУ')),
                ('drfo', models.CharField(blank=True, max_length=11, verbose_name='ДРФО')),
                ('serial', models.CharField(max_length=50, unique=True, verbose_name='Серийный номер')),
                ('valid_to', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Действителен до')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'certificate',
            },
        ),
        migrations.CreateModel(
            name='Issuer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500)),
                ('address', models.CharField(default='', max_length=255)),
                ('ocsp_access_point_address', models.CharField(default='', max_length=255)),
                ('ocsp_access_point_port', models.CharField(default='', max_length=5)),
                ('cmp_address', models.CharField(default='', max_length=100)),
                ('tsp_address', models.CharField(default='', max_length=100)),
                ('tsp_address_port', models.CharField(default='', max_length=5)),
                ('direct_access', models.BooleanField(default=False)),
                ('certs_in_key', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'issuer',
            },
        ),
    ]
