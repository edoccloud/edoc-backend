from django.db import models
from edoc.account.models import Account
from django.utils import timezone


class Issuer(models.Model):

    name = models.CharField(max_length=500)
    address = models.CharField(max_length=255, default='')
    ocsp_access_point_address = models.CharField(max_length=255, default='')
    ocsp_access_point_port = models.CharField(max_length=5, default='')
    cmp_address = models.CharField(max_length=100, default='')
    tsp_address = models.CharField(max_length=100, default='')
    tsp_address_port = models.CharField(max_length=5, default='')
    direct_access = models.BooleanField(default=False)
    certs_in_key = models.BooleanField(default=False)

    def short_name(self):
        return self.name.split(',')[0]

    class Meta:
        db_table = 'issuer'


class Certificate(models.Model):

    issuer_index = models.IntegerField('Индекс АЦСК', default=0)
    organization = models.CharField('Название организации', max_length=100, blank=True)
    name = models.CharField('ФИО', max_length=255, blank=True)
    edrpou = models.CharField('ЕДРПОУ', max_length=11, blank=True)
    drfo = models.CharField('ДРФО', max_length=11, blank=True)
    serial = models.CharField('Серийный номер', max_length=50)
    valid_to = models.DateTimeField('Действителен до', default=timezone.now)
    user = models.ForeignKey(Account, null=True, blank=True, on_delete=models.CASCADE)
    title = models.CharField('Должность', max_length=63, blank=True)

    def __repr__(self):
        return '{} {}'.format(self.organization, self.name)

    def __str__(self):
        return '{} {}'.format(self.organization, self.name)

    class Meta:
        unique_together = ('serial', 'user')
        db_table = 'certificate'
