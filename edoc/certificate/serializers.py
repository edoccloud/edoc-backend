from rest_framework import serializers

from edoc.certificate.models import Certificate


class CertificateSerializer(serializers.ModelSerializer):
    issuer_index = serializers.IntegerField()
    organization = serializers.CharField()
    name = serializers.CharField()
    edrpou = serializers.CharField(allow_blank=True)
    drfo = serializers.CharField()
    serial = serializers.CharField()
    valid_to = serializers.DateTimeField()
    title = serializers.CharField()

    class Meta:
        model = Certificate
        fields = ('issuer_index', 'organization', 'name', 'edrpou', 'drfo', 'serial', 'valid_to', 'title')
