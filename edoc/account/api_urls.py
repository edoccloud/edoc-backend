from django.urls import path
from edoc.account.api_views import RegApi
from edoc.account.api_views import RegConfirmApi
from edoc.account.api_views import AuthApi
from edoc.account.api_views import ProfileApi
from edoc.account.api_views import RecoverSmsApi
from edoc.account.api_views import RecoverChangePasswordApi


urlpatterns = [
    path('reg/', RegApi.as_view()),
    path('reg/confirm/', RegConfirmApi.as_view()),
    path('auth/', AuthApi.as_view()),
    path('profile/', ProfileApi.as_view()),
    path('recover/sms/', RecoverSmsApi.as_view()),
    path('recover/password/', RecoverChangePasswordApi.as_view())
]