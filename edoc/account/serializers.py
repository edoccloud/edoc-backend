from django.contrib.auth import authenticate
from rest_framework import serializers

from edoc.account.models import Account
from edoc.account.validators import validate_phone
from edoc.sms.models import Sms


class AuthSerializer(serializers.Serializer):
    phone = serializers.CharField(validators=[validate_phone])
    password = serializers.CharField()

    def validate(self, data):
        self.user = authenticate(phone=data['phone'], password=data['password'])
        if not self.user:
            raise serializers.ValidationError({
                'phone': 'Неверный телефон или пароль.'})

        return data


class RegSerializer(serializers.Serializer):
    phone = serializers.CharField(validators=[validate_phone])
    password = serializers.CharField(
        min_length=6,
        error_messages={'min_length': 'Минимальная длина пароля {min_length} символов'})
    password2 = serializers.CharField(
        min_length=6,
        error_messages={'min_length': 'Минимальная длина пароля {min_length} символов'}
    )

    def validate(self, data):
        if data['password'] != data['password2']:
            raise serializers.ValidationError({'password2': 'Пароли на совпадают.'})

        if Account.objects.filter(phone=data['phone']):
            raise serializers.ValidationError({'phone': 'Пользователь с таким телефоном уже зарегистрирован'})

        return data


class RegConfirmSerializer(serializers.Serializer):
    code = serializers.CharField()
    phone = serializers.CharField(validators=[validate_phone])
    password = serializers.CharField(
        min_length=6,
        error_messages={'min_length': 'Минимальная длина пароля {min_length} символов'})

    def validate(self, data):
        if not Sms.objects.filter(phone=data['phone'], code=data['code']):
            raise serializers.ValidationError({'code': 'Неверно введён смс-код.'})

        return data


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = ('phone',)


class RecoverSmsSerializer(serializers.Serializer):
    phone = serializers.CharField(validators=[validate_phone])

    def validate(self, data):
        if not Account.objects.filter(phone=data['phone']):
            raise serializers.ValidationError({
                'phone': 'Пользователь с данным номером телефона не найден.'})

        return data


class RecoverChangePasswordSerializer(serializers.Serializer):
    phone = serializers.CharField(validators=[validate_phone])
    code = serializers.CharField(
        min_length=4,
        max_length=4,
        error_messages={
            'min_length': 'Смс-код состоит из {min_length} цифр.',
            'max_length': 'Смс-код состоит из {max_length} цифр.'
        }
    )
    password = serializers.CharField(
        min_length=6,
        error_messages={'min_length': 'Минимальная длина пароля {min_length} символов'})

    def validate(self, data):
        self.sms = Sms.objects.filter(phone=data['phone'], code=data['code'])
        if not self.sms:
            raise serializers.ValidationError({'code': 'Неверно введён смс-код или номер телефона.'})

        return data
