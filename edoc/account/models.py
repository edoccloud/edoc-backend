from django.contrib.auth.models import UserManager
from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models


class Account(AbstractBaseUser):
    phone = models.CharField(max_length=12, unique=True)

    balance = models.DecimalField(max_digits=12, decimal_places=2, default=0)

    USERNAME_FIELD = 'phone'

    objects = UserManager()

    def __str__(self):
        return '{}'.format(self.phone)

    class Meta:
        db_table = 'account'


class Token(models.Model):

    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    token = models.UUIDField(primary_key=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.token, self.created_at)

    class Meta:
        db_table = 'token'
