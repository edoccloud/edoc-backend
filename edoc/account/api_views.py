import uuid
import random
import string

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from edoc.account.models import Account
from edoc.account.serializers import AuthSerializer
from edoc.account.serializers import RegSerializer
from edoc.account.serializers import RegConfirmSerializer
from edoc.account.serializers import ProfileSerializer
from edoc.account.serializers import RecoverSmsSerializer
from edoc.account.serializers import RecoverChangePasswordSerializer
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from edoc.utils.authenticate import TokenAuthentication

from edoc.sms.models import Sms
from edoc.sms.utils import send_sms


class RegApi(APIView):

    def post(self, request):
        serializer = RegSerializer(data=request.data)
        if serializer.is_valid():
            code = ''.join([random.choice(string.digits) for _ in range(4)])

            sms_error, status_code = send_sms(serializer.validated_data['phone'], 'Код подтверждения: {}'.format(code))
            if status_code == 400:
                return Response(
                    {'errors':
                        {
                            'phone': 'Ошибка при отправке смс - {}'.format(sms_error)
                        }
                    },
                    status=status.HTTP_400_BAD_REQUEST)

            new_sms, created = Sms.objects.get_or_create(phone=serializer.validated_data['phone'])
            new_sms.code = code
            new_sms.save()
            return Response({'status': 'OK'}, status=status.HTTP_200_OK)

        return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class RegConfirmApi(APIView):

    def post(self, request):
        serializer = RegConfirmSerializer(data=request.data)
        if serializer.is_valid():
            phone = serializer.validated_data['phone']
            new_account = Account.objects.create(phone=phone)
            new_account.set_password(serializer.validated_data['password'])
            new_account.save()

            new_token = new_account.token_set.create(token=uuid.uuid4())

            # clear activated sms
            Sms.objects.filter(phone=phone).delete()

            return Response({'token': new_token.token}, status=status.HTTP_200_OK)
        return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class AuthApi(APIView):

    def post(self, request):
        serializer = AuthSerializer(data=request.data)
        if serializer.is_valid():

            new_token = serializer.user.token_set.create(token=uuid.uuid4())

            return Response({'token': new_token.token}, status=status.HTTP_200_OK)
        return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class ProfileApi(generics.GenericAPIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = Account.objects.get(pk=request.user.pk)
        return Response(ProfileSerializer(profile).data, status=status.HTTP_200_OK)


class RecoverSmsApi(generics.GenericAPIView):

    def post(self, request):

        serializer = RecoverSmsSerializer(data=request.data)
        if serializer.is_valid():

            code = ''.join([random.choice(string.digits) for _ in range(4)])

            sms_error, status_code = send_sms(serializer.validated_data['phone'], 'Код для сброса пароля: {}'.format(code))
            if status_code == 400:
                return Response(
                    {
                        'message': 'Ошибка при отправке СМС.',
                        'errors': [{
                            'field': 'phone',
                            'message': 'Ошибка при отправке смс - {}'.format(sms_error)
                        }]
                    },
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            new_sms, created = Sms.objects.get_or_create(phone=serializer.validated_data['phone'])
            new_sms.code = code
            new_sms.save()

            return Response({})

        errors = []
        for k, v in serializer.errors.items():
            errors.append({
                'field': k,
                'message': v
            })

        return Response({
            'message': 'Ошибка валидации', 
            'errors': errors
        }, status=status.HTTP_422_UNPROCESSABLE_ENTITY)


class RecoverChangePasswordApi(generics.GenericAPIView):

    def post(self, request):

        serializer = RecoverChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            phone = serializer.validated_data['phone']
            password = serializer.validated_data['password']

            account = Account.objects.get(phone=phone)
            account.set_password(password)
            account.save()

            serializer.sms.delete()

            return Response({})

        errors = []
        for k, v in serializer.errors.items():
            errors.append({
                'field': k,
                'message': v
            })

        return Response({
            'message': 'Ошибка валидации', 
            'errors': errors
        }, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
