import re
from rest_framework import serializers


def validate_phone(value):
    if not re.match('^380[0-9]{9}$', value):
        raise serializers.ValidationError('Неверный формат телефона.')
