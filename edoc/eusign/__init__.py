import os
from ctypes import *
from django.conf import settings
from edoc.eusign import EUSignCP


for filename in os.listdir(os.path.join(settings.BASE_DIR, 'eusign_libs')):
    if '.so' not in filename:
        continue
    cdll.LoadLibrary(os.path.join(settings.BASE_DIR, 'eusign_libs', filename))

EUSignCP.EULoad()
