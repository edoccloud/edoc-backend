from django.urls import path
from edoc.organization.api_views import OrganizationListApi
from edoc.organization.api_views import OrganizationDetailApi


urlpatterns = [
    path('', OrganizationListApi.as_view()),
    path('<int:pk>', OrganizationDetailApi.as_view())
]