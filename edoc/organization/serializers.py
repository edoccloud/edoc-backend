from rest_framework import serializers

from edoc.organization.models import Organization


class OrganizationSerializer(serializers.ModelSerializer):

    drfo = serializers.CharField(allow_blank=True)

    class Meta:
        model = Organization
        exclude = ('user',)
