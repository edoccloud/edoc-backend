from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from edoc.utils.authenticate import TokenAuthentication
from edoc.organization.models import Organization
from edoc.organization.serializers import OrganizationSerializer


class OrganizationListApi(generics.GenericAPIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = OrganizationSerializer

    def get(self, request):
        organizations = request.user.organizations.all().order_by('-id')

        serializer = OrganizationSerializer(organizations, many=True)

        return Response({'data': serializer.data}, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = OrganizationSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

        new_organization = Organization(
            name=serializer.validated_data['name'],
            edrpou=serializer.validated_data['edrpou'],
            drfo=serializer.validated_data['drfo'],
            user=request.user
        )
        new_organization.save()

        return Response(OrganizationSerializer(new_organization).data, status=status.HTTP_201_CREATED)


class OrganizationDetailApi(generics.DestroyAPIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Organization.objects.filter(user=self.request.user)
