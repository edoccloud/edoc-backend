from django.db import models
from edoc.account.models import Account


class Organization(models.Model):

    name = models.CharField(max_length=100)
    edrpou = models.CharField(max_length=11, default='')
    drfo = models.CharField(max_length=11, default='')
    user = models.ForeignKey(Account, related_name='organizations', on_delete=models.CASCADE)

    class Meta:
        db_table = 'organization'
