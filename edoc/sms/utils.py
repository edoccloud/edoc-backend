import requests
from django.conf import settings
from zeep import Client


def send_sms(phone, message):

    client = Client('http://turbosms.in.ua/api/wsdl.html')

    result = client.service.Auth('denekurich', '472924')
    print(result)
    if result != 'Вы успешно авторизировались':
        return 400, 'Ошибка сервиса свяжитесь с администрацией'

    result = client.service.SendSMS(
        'edoc.cloud',
        '+{}'.format(phone),
        message
    )
    print(result)

    if result[0] != 'Сообщения успешно отправлены':
        return 400, result[0]

    return 200, 'OK'
