from django.db import models


class Sms(models.Model):

    phone = models.CharField(max_length=12)
    code = models.CharField(max_length=4)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} {}'.format(self.phone, self.code)

    class Meta:
        db_table = 'sms'
