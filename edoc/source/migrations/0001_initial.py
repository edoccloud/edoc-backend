# Generated by Django 2.1 on 2018-11-15 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField()),
                ('status', models.CharField(choices=[('active', 'Активный'), ('blocked', 'Заблокирован')], max_length=10)),
            ],
            options={
                'db_table': 'source',
            },
        ),
    ]
