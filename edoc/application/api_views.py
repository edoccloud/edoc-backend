from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from edoc.application.models import Application
from edoc.utils.authenticate import TokenAuthentication
from edoc.application.serializers import ApplicationSerializer


class ApplicationListApi(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):

        q = Q(edrpou='', drfo='')
        for certificate in request.user.certificate_set.all():
            q |= Q(edrpou=certificate.edrpou)
            q |= Q(drfo=certificate.drfo)

        applications = Application.objects.filter(q)

        serializer = ApplicationSerializer(applications, many=True)

        return Response({'data': serializer.data}, status=status.HTTP_200_OK)
