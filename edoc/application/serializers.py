from rest_framework import serializers


class ApplicationSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.CharField()
    edrpou = serializers.CharField()
    drfo = serializers.CharField()