import uuid
from django.db import models
from edoc.account.models import Account


class Application(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    edrpou = models.CharField(max_length=11, db_index=True, default='')
    drfo = models.CharField(max_length=11, db_index=True, default='')

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'application'
