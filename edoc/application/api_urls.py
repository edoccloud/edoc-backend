from django.urls import path
from edoc.application.api_views import ApplicationListApi


urlpatterns = [
    path('', ApplicationListApi.as_view()),
]