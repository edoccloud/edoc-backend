import requests
import base64

from django.http import HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator



@method_decorator(csrf_exempt, name='dispatch')
class ProxyView(View):

    def get_content_type(self, address):

        if '/services/cmp' in address:
            return ''

        if ('/services/ocsp' in address) or ('/public/ocsp' in address):
            return 'application/ocsp-request'

        if '/services/tsp' in address:
            return 'application/timestamp-query'

        return 'text/plain'

    def post(self, request, *args, **kwargs):
        address = request.GET.get('address')

        data = base64.b64decode(request.body)

        headers = {
            'User-Agent': 'signature.proxy',
            'Content-Type': self.get_content_type(address),
        }

        req = requests.post(address, data=data, headers=headers)

        response = HttpResponse(base64.b64encode(req.content),
                                content_type='X-user/base64-data')

        response['Cache-Control'] = 'no-store, no-cache, must-revalidate'
        return response