# Generated by Django 2.1 on 2018-09-07 12:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0002_document_share_code'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='share_code',
        ),
        migrations.AddField(
            model_name='document',
            name='is_shared',
            field=models.BooleanField(default=False),
        ),
    ]
