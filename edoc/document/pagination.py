from collections import OrderedDict

from rest_framework.pagination import CursorPagination
from rest_framework.response import Response


class DocumentPagination(CursorPagination):
    ordering = '-created_at'

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('data', data)
        ]))
