from rest_framework import serializers
from edoc.document.models import Document
from edoc.document.models import Sign
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser
from django.conf import settings


class SignSerializer(serializers.ModelSerializer):

    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        request = self.context['request']
        return '{}://{}{}'.format(
            request.scheme,
            request.get_host(),
            reverse('sign_get_api', args=[obj.document.id, '{}/{}'.format(obj.drfo, obj.edrpou)])
        )

    class Meta:
        model = Sign
        exclude = ('id', 'document')


class DocumentSerializer(serializers.ModelSerializer):
    signs = SignSerializer(many=True, read_only=True)

    status = serializers.SerializerMethodField()
    view_url = serializers.SerializerMethodField()
    archive_url = serializers.SerializerMethodField()
    original_url = serializers.SerializerMethodField()
    share_url = serializers.SerializerMethodField()

    is_archived = serializers.SerializerMethodField()
    is_seen = serializers.SerializerMethodField()

    class Meta:
        model = Document
        exclude = (
            'sender_archived_at',
            'sender_deleted_at',
            'sender_is_archived',
            'sender_is_deleted',
            'sender_status',
            'sender_is_seen',
            'receiver_archived_at',
            'receiver_deleted_at',
            'receiver_is_archived',
            'receiver_is_deleted',
            'receiver_status',
            'receiver_is_seen'
        )

    def get_is_seen(self, obj):
        if type(self.context['request'].user) is not AnonymousUser \
                and obj.sender_phone == self.context['request'].user.phone:
            return obj.sender_is_seen
        else:
            return obj.receiver_is_seen

    def get_status(self, obj):
        if type(self.context['request'].user) is not AnonymousUser \
                and obj.sender_phone == self.context['request'].user.phone:
            return obj.sender_status
        else:
            return obj.receiver_status

    def get_is_archived(self, obj):
        if type(self.context['request'].user) is not AnonymousUser \
                and obj.sender_phone == self.context['request'].user.phone:
            return obj.sender_is_archived
        else:
            return obj.receiver_is_archived

    def get_share_url(self, obj):
        request = self.context['request']
        if not obj.is_shared:
            return ''

        return '{}://{}/shared/{}'.format(
            request.scheme,
            settings.APP_URL,
            obj.pk)

    def get_archive_url(self, obj):
        request = self.context['request']
        return '{}://{}{}'.format(
            request.scheme,
            request.get_host(),
            reverse('archive_get_api', args=[obj.id])
        )

    def get_original_url(self, obj):
        request = self.context['request']
        return '{}://{}{}'.format(
            request.scheme,
            request.get_host(),
            reverse('original_get_api', args=[obj.id])
        )

    def get_view_url(self, obj):
        request = self.context['request']
        return '{}://{}{}'.format(
            request.scheme,
            request.get_host(),
            reverse('view_get_api', args=[obj.id])
        )


class DocumentSendSerializer(serializers.Serializer):

    edrpou = serializers.CharField(allow_blank=True, default='')
    drfo = serializers.CharField(allow_blank=True, default='')

    def validate(self, data):

        errors = {}

        if not (data['edrpou'] or data['drfo']):
            errors.update({'common': 'Неверно указан получатель.'})

        if errors:
            raise serializers.ValidationError(errors)

        return data
