from django import forms
from edoc.certificate.models import Certificate
from edoc.application.models import Application
from django.forms import widgets


class DocumentPostAPIForm(forms.Form):
    document = forms.FileField()
    is_shared = forms.BooleanField(initial=False, required=False)

    def clean_is_shared(self):
        return self.cleaned_data.get('is_shared', False)


class DocumentSignAPIForm(forms.Form):
    sign = forms.FileField()
    is_external = forms.BooleanField(required=False)


class DocumentCreateForm(forms.Form):

    certificate = forms.ModelChoiceField(
        queryset=Certificate.objects.all(), empty_label="(Не выбран)", initial=0, widget=widgets.Select(attrs={
            'class': 'form-control',
            'id': 'certificate'}))
    to_application = forms.ModelChoiceField(
        queryset=Application.objects.all(), empty_label="(Не выбран)", initial='', widget=widgets.Select(attrs={
            'class': 'form-control',
            'id': 'certificate'}))
    to_edrpou = forms.CharField(max_length=10, widget=widgets.TextInput(attrs={
        'class': 'form-control',
        'id': 'to_edrpou'}), required=False)
    to_drfo = forms.CharField(max_length=10, required=False, widget=widgets.TextInput(attrs={
        'class': 'form-control',
        'id': 'to_drfo'}))
    document = forms.FileField(widget=widgets.FileInput(attrs={
        'class': 'form-control',
        'id': 'document'
    }))

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(DocumentCreateForm, self).__init__(*args, **kwargs)
        self.fields['certificate'].queryset = Certificate.objects.filter(user=self.request.user)


class DocumentSignForm(forms.Form):

    sign = forms.FileField()
    is_external = forms.BooleanField()
