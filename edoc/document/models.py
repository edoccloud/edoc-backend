import uuid
from django.db import models
from edoc.application.models import Application
from edoc.account.models import Account


class Document(models.Model):
    
    STATUSES = (
        ('pending', 'Загружен'),
        ('sent', 'Отправлен'),
        ('inbox', 'Получен')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, default='', db_index=True)
    size = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    sent_at = models.DateTimeField(blank=True, null=True)

    sender_status = models.CharField(max_length=8, choices=STATUSES, db_index=True, default='pending')
    sender_name = models.CharField(max_length=100, db_index=True, default='')
    sender_organization = models.CharField(max_length=100, default='')
    sender_edrpou = models.CharField(max_length=11, db_index=True, default='')
    sender_drfo = models.CharField(max_length=11, db_index=True, default='')
    sender_phone = models.CharField(max_length=12, db_index=True, default='')
    sender_application = models.ForeignKey(
        Application,
        default='075d3ecd-819b-4c08-acc6-97f4f99784d2',
        related_name='documents_from_application',
        on_delete=models.CASCADE)
    sender_is_archived = models.BooleanField(default=False)
    sender_archived_at = models.DateTimeField(blank=True, null=True)
    sender_is_deleted = models.BooleanField(default=False)
    sender_deleted_at = models.DateTimeField(blank=True, null=True)

    receiver_status = models.CharField(max_length=8, choices=STATUSES, db_index=True, default='pending')
    receiver_name = models.CharField(max_length=100, db_index=True, default='')
    receiver_organization = models.CharField(max_length=100, db_index=True, default='')
    receiver_edrpou = models.CharField(max_length=11, db_index=True, default='')
    receiver_drfo = models.CharField(max_length=11, db_index=True, default='')
    receiver_phone = models.CharField(max_length=12, db_index=True, default='')
    receiver_application = models.ForeignKey(
        Application,
        blank=True,
        null=True,
        related_name='documents_to_application',
        on_delete=models.CASCADE)
    receiver_is_archived = models.BooleanField(default=False)
    receiver_archived_at = models.DateTimeField(blank=True, null=True)
    receiver_is_deleted = models.BooleanField(default=False)
    receiver_deleted_at = models.DateTimeField(blank=True, null=True)

    is_shared = models.BooleanField(default=False)
    sender_is_seen = models.BooleanField(default=False)
    receiver_is_seen = models.BooleanField(default=False)

    class Meta:
        db_table = 'document'

"""
class Message(models.Model):

    document = models.ForeignKey(Document)

    class Meta:
        db_table = 'message'
"""

class Sign(models.Model):

    document = models.ForeignKey(Document, related_name='signs', on_delete=models.CASCADE)
    edrpou = models.CharField(max_length=11, db_index=True, default='')
    drfo = models.CharField(max_length=11, db_index=True)
    name = models.CharField(max_length=100, db_index=True)
    serial = models.CharField(max_length=100, default='')
    filename = models.CharField(max_length=100, default='')
    organization = models.CharField(max_length=100, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_external = models.BooleanField(default=False)
    size = models.IntegerField(default=0)
    title = models.CharField(max_length=100, default='')

    class Meta:
        db_table = 'document_sign'
