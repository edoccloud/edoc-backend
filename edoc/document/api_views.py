import io
from datetime import datetime
from urllib.parse import quote
from translitua import translit
from zipfile import ZipFile
from zipfile import ZIP_LZMA

from django.db.models import Q
from django.http import HttpResponse
from django.utils import timezone
from django.contrib.auth.models import AnonymousUser

from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from google.cloud.storage.blob import Blob
from google.cloud import storage

from edoc.document.forms import DocumentPostAPIForm
from edoc.document.forms import DocumentSignAPIForm
from edoc.document.serializers import DocumentSerializer
from edoc.document.serializers import DocumentSendSerializer
from edoc.document.models import Document
from edoc.document.models import Sign
from edoc.utils.authenticate import TokenAuthentication
from edoc.utils.authenticate import TokenFromGetAuthentication
from edoc.document.pagination import DocumentPagination

from edoc.eusign import EUSignCP


class SenderReceiverQuerysetMixin:
    def get_sender_queryset(self):
        return Q(sender_phone=self.request.user.phone)

    def get_receiver_queryset(self):
        self.certificates = list(self.request.user.certificate_set.all())

        q = Q()
        if len(self.certificates) > 0:
            for certificate in self.certificates:
                if certificate.edrpou:
                    q |= Q(receiver_edrpou=certificate.edrpou)
                if certificate.drfo:
                    q |= Q(receiver_drfo=certificate.drfo)

        return q


class DocumentDetailApi(generics.RetrieveDestroyAPIView,
                        SenderReceiverQuerysetMixin):

    authentication_classes = (TokenAuthentication,)
    serializer_class = DocumentSerializer

    def get_queryset(self):

        q = Q(is_shared=True)
        if type(self.request.user) is not AnonymousUser:
            q |= Q(self.get_sender_queryset() | self.get_receiver_queryset())

        return Document.objects.filter(q)

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        if type(self.request.user) is not AnonymousUser:

            if instance.sender_phone == self.request.user.phone:
                instance.sender_is_seen = True
            else:
                certificates = list(request.user.certificate_set.all())
                instance.receiver_is_seen = False
                for certificate in certificates:
                    if certificate.edrpou == instance.receiver_edrpou or \
                       certificate.drfo == instance.receiver_drfo:
                        instance.receiver_is_seen = True
                        break

            instance.save()

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @permission_classes(IsAuthenticated)
    def delete(self, request, *args, **kwargs):

        certificates = list(request.user.certificate_set.all())

        q1 = Q(Q(sender_status='pending') | Q(sender_is_archived=True), sender_phone=request.user.phone)
        q2 = Q()
        cert_count = 0
        for certificate in certificates:
            if certificate.edrpou:
                q2 |= Q(receiver_edrpou=certificate.edrpou)
            if certificate.drfo:
                q2 |= Q(receiver_drfo=certificate.drfo)
            cert_count += 1
        if cert_count > 0:
            q2 = Q(q2, receiver_is_archived=True)

        q = Q(q1) | Q(q2)

        document = Document.objects.filter(q, id=kwargs['pk']).first()

        if not document:
            return Response({'errors': {
                'common': 'Документ не найден или находится в статусе недопустимом для удаления.'}},
                status=status.HTTP_404_NOT_FOUND)

        if document.sender_phone == request.user.phone:
            document.sender_is_deleted = True
            document.sender_archived_at = timezone.now()

        for certificate in certificates:
            if certificate.edrpou == document.receiver_edrpou \
                or certificate.drfo == document.receiver_drfo:
                document.receiver_is_deleted = True
                document.receiver_archived_at = timezone.now()
                break

        document.save()

        return Response(
            DocumentSerializer(
                document, context=self.get_serializer_context()).data,
            status=status.HTTP_200_OK
        )


class DocumentListApi(generics.ListCreateAPIView):

    serializer_class = DocumentSerializer
    authentication_classes = (TokenAuthentication,)
    pagination_class = DocumentPagination

    def get_queryset(self):

        mode = self.kwargs['mode']
        certificates = []
        for certificate in self.request.user.certificate_set.all():
            certificates.append({
                'edrpou': certificate.edrpou,
                'drfo': certificate.drfo
            })

        if mode == 'inbox':
            if len(certificates) == 0:
                return Document.objects.none()
            q = Q()
            for certificate in certificates:
                if certificate['edrpou']:
                    q |= Q(receiver_edrpou=certificate['edrpou'])
                if certificate['drfo']:
                    q |= Q(receiver_drfo=certificate['drfo'])
            q = Q(q, receiver_status='inbox', receiver_is_archived=False, receiver_is_deleted=False)
        elif mode == 'sent':
            q = Q(sender_status='sent',
                  sender_phone=self.request.user.phone,
                  sender_is_archived=False,
                  sender_is_deleted=False)
        elif mode == 'pending':
            q = Q(sender_status='pending',
                  sender_phone=self.request.user.phone,
                  sender_is_archived = False,
                  sender_is_deleted = False)
        elif mode == 'archive':
            q1 = Q(sender_phone=self.request.user.phone, sender_is_archived=True, sender_is_deleted=False)
            q2 = Q()
            for certificate in certificates:
                if certificate['edrpou']:
                    q2 |= Q(receiver_edrpou=certificate['edrpou'])
                if certificate['drfo']:
                    q2 |= Q(receiver_drfo=certificate['drfo'])
            if len(certificates) > 0:
                q2 = Q(q2, receiver_is_archived=True, receiver_is_deleted=False)
            q = Q(q1) | Q(q2)
        else:
            q1 = Q()
            if len(certificates) > 0:
                for certificate in certificates:
                    if certificate['edrpou']:
                        q1 |= Q(receiver_edrpou=certificate['edrpou'])
                    if certificate['drfo']:
                        q1 |= Q(receiver_drfo=certificate['drfo'])
                q1 = Q(q1, receiver_is_deleted=False)
            q2 = Q(sender_is_deleted=False, sender_phone=self.request.user.phone)
            q = Q(q1) | Q(q2)

        search = self.request.query_params.get('search', None)
        if search is not None:
            q &= Q(
                Q(sender_edrpou__contains=search)
                | Q(sender_drfo__contains=search)
                | Q(receiver_edrpou__contains=search)
                | Q(receiver_drfo__contains=search)
                | Q(name__contains=search)
            )

        start_date = self.request.query_params.get('start_date', None)
        if start_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            start_date = timezone.make_aware(start_date)
            q &= Q(created_at__gte=start_date)

        end_date = self.request.query_params.get('end_date', None)
        if end_date:
            end_date = datetime.strptime(end_date, '%Y-%m-%d')
            end_date = datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
            end_date = timezone.make_aware(end_date)
            q &= Q(created_at__lte=end_date)

        return Document.objects.prefetch_related('signs').filter(q)

    @permission_classes(IsAuthenticated)
    def get(self, request, mode):
        if type(request.user) is AnonymousUser:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        l = self.list(request)
        return l

    def post(self, request, *args, **kwargs):

        form = DocumentPostAPIForm(request.POST, request.FILES)

        if not form.is_valid():
            return Response({'errors': {k: v for k,v in form.errors.items()}})

        if form.cleaned_data['document'].size > 10 * 1024 * 1024: # more than 10Mb
            return Response({'error': 'Файл больше 10 Мб'}, status=status.HTTP_400_BAD_REQUEST)

        StorageClient = storage.Client()

        document = form.cleaned_data['document']
        data = {
            'name': document.name,
            'size': document.size
        }

        if type(self.request.user) is not AnonymousUser:
            data.update({
                'sender_phone': request.user.phone,
                'is_shared': form.cleaned_data.get('is_shared', False)
            })
        else:
            data.update({
                'is_shared': True
            })

        new_document = Document(**data)
        new_document.save()

        bucket = StorageClient.get_bucket('edoc-cloud')
        blob = Blob('{}/{}'.format(new_document.id, document.name), bucket)
        blob.upload_from_file(document.file)

        return Response(DocumentSerializer(new_document, context=self.get_serializer_context()).data,
                        status=status.HTTP_201_CREATED)


class DocumentSignApi(generics.GenericAPIView,
                      SenderReceiverQuerysetMixin):

    authentication_classes = (TokenAuthentication,)

    def post(self, request, document_id):

        if type(request.user) is AnonymousUser:
            q = Q(is_shared=True)
        else:
            q = self.get_sender_queryset() | self.get_receiver_queryset() | Q(is_shared=True)

        document = Document.objects.filter(q, id=document_id).first()

        if not document:
            return Response([], status=status.HTTP_403_FORBIDDEN)

        form = DocumentSignAPIForm(request.POST, request.FILES)
        if not form.is_valid():
            return Response({'errors': dict(form.errors)}, status=status.HTTP_400_BAD_REQUEST)

        sign = form.cleaned_data['sign'].read()
        is_external = form.cleaned_data['is_external']

        eusign = EUSignCP.EUGetInterface()
        eusign.Initialize()

        cert_bytes = bytes()
        signer_info = {}

        try:
            eusign.GetSignerInfo(0, None, sign, len(sign), signer_info, cert_bytes)
        except RuntimeError:
            return Response({'errors': {
                'common': 'Загруженный файл не является подписью.'
            }}, status=status.HTTP_400_BAD_REQUEST)

        StorageClient = storage.Client()
        bucket = StorageClient.get_bucket('edoc-cloud')

        print(signer_info)

        organization = signer_info['pszSubjOrg']
        edrpou = signer_info['pszSubjEDRPOUCode']
        drfo = signer_info['pszSubjDRFOCode']
        name = signer_info['pszSubjCN']
        serial = signer_info['pszSerial']
        title = signer_info['pszSubjTitle']

        if edrpou:
            file_name = 'Pidpys_{}_{}_{}.p7s'.format(
                translit(name).replace(' ', '_'), edrpou, drfo)
        else:
            file_name = 'Pidpys_{}_{}.p7s'.format(
                translit(name).replace(' ', '_'), drfo)

        sign_file_name = '{}/{}'.format(document_id, file_name)
        
        blob = Blob(sign_file_name, bucket)
        blob.upload_from_file(io.BytesIO(sign))

        try:
            sign_object = Sign.objects.get(document_id=document_id, edrpou=edrpou, drfo=drfo, is_external=is_external)
        except Sign.DoesNotExist:
            new_signature = Sign(
                document_id=document_id,
                edrpou=edrpou,
                organization=organization,
                drfo=drfo,
                name=name,
                serial=serial,
                filename=file_name,
                is_external=is_external,
                title=title,
                size=len(sign)
            )
            new_signature.save()

        return Response(DocumentSerializer(document,
                                           context={'request': self.request}).data,
                        status=status.HTTP_201_CREATED)


class DocumentDownloadSignApi(generics.GenericAPIView,
                              SenderReceiverQuerysetMixin):

    authentication_classes = (TokenFromGetAuthentication,)

    def get(self, request, *args, **kwargs):

        if type(request.user) is AnonymousUser:
            q = Q(is_shared=True)
        else:
            q = self.get_sender_queryset() | self.get_receiver_queryset() | Q(is_shared=True)

        document = Document.objects.filter(q, id=kwargs['document_id']).first()

        if not document:
            return Response({'errors': {'common': 'Документ не найден.'}}, status=status.HTTP_404_NOT_FOUND)

        drfo, edrpou = kwargs['drfo_edrpou'].split('/')

        sign = document.signs.filter(edrpou=edrpou, drfo=drfo).first()

        if not sign:
            return Response('404 Not found', status=status.HTTP_404_NOT_FOUND)

        StorageClient = storage.Client()
        bucket = StorageClient.get_bucket('edoc-cloud')
        f = bucket.blob('{}/{}'.format(document.id, sign.filename))
        data = f.download_as_string()

        response = HttpResponse(data, content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(quote(sign.filename))

        return response


class DocumentSendApi(generics.GenericAPIView,
                      SenderReceiverQuerysetMixin):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def put(self, request, document_id):

        q = self.get_sender_queryset()

        try:
            document = Document.objects.get(q, id=document_id)
        except Document.DoesNotExist:
            return Response([], status=status.HTTP_404_NOT_FOUND)

        if document.sender_status == 'sent':
            return Response({'errors': {'common': ['Документ уже был отправлен']}}, status=status.HTTP_400_BAD_REQUEST)

        serializer = DocumentSendSerializer(data=request.data)

        if not serializer.is_valid():
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

        document.from_phone = request.user.phone
        document.receiver_edrpou = serializer.data['edrpou']
        document.receiver_drfo = serializer.data['drfo']
        document.receiver_status = 'inbox'
        document.sender_status = 'sent'
        document.sent_at = timezone.now()
        document.save()

        return Response(
            DocumentSerializer(
                document, context=self.get_serializer_context()).data,
            status=status.HTTP_200_OK
        )


class DocumentGetZipArchiveApi(generics.GenericAPIView,
                               SenderReceiverQuerysetMixin):

    authentication_classes = (TokenFromGetAuthentication,)

    def get(self, request, *args, **kwargs):

        if type(request.user) is AnonymousUser:
            q = Q(is_shared=True)
        else:
            q = self.get_sender_queryset() | self.get_receiver_queryset() | Q(is_shared=True)

        document = Document.objects.filter(q, id=kwargs['document_id']).first()

        if not document:
            return Response({'errors': {'common': 'Документ не найден.'}}, status=status.HTTP_404_NOT_FOUND)

        StorageClient = storage.Client()
        bucket = StorageClient.get_bucket('edoc-cloud')
        f = bucket.blob('{}/{}'.format(document.id, document.name))
        data = f.download_as_string()

        s = io.BytesIO()

        # write documents
        zf = ZipFile(s, "w", compression=ZIP_LZMA)
        zf.writestr(document.name, data)

        # write signatures
        for sign in document.signs.all():
            f = bucket.blob('{}/{}'.format(document.id, sign.filename))
            data = f.download_as_string()
            zf.writestr(sign.filename, data)

        zf.close()

        response = HttpResponse(s.getvalue(), content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename="{}.zip"; filename*=UTF-8\'\'{}.zip'.format(
            quote(document.name),
            quote(document.name))

        return response


class DocumentOriginalApi(generics.GenericAPIView,
                          SenderReceiverQuerysetMixin):

    authentication_classes = (TokenFromGetAuthentication,)

    def get(self, request, *args, **kwargs):

        if type(request.user) is AnonymousUser:
            q = Q(is_shared=True)
        else:
            q = self.get_receiver_queryset() | self.get_sender_queryset() | Q(is_shared=True)

        document = Document.objects.filter(q, id=kwargs['document_id']).first()

        if not document:
            return Response({'errors': {'common': 'Документ не найден.'}}, status=status.HTTP_404_NOT_FOUND)

        StorageClient = storage.Client()
        bucket = StorageClient.get_bucket('edoc-cloud')
        f = bucket.blob('{}/{}'.format(document.id, document.name))
        data = f.download_as_string()
        response = HttpResponse(data, content_type=f.content_type)
        response['Content-Disposition'] = 'attachment; filename="{}"; filename*=UTF-8\'\'{}'.format(
            quote(document.name),
            quote(document.name))

        return response


class DocumentViewApi(generics.GenericAPIView,
                      SenderReceiverQuerysetMixin):

    authentication_classes = (TokenFromGetAuthentication,)

    def get(self, request, *args, **kwargs):

        if type(request.user) is AnonymousUser:
            q = Q(is_shared=True)
        else:
            q = self.get_sender_queryset() | self.get_receiver_queryset() | Q(is_shared=True)

        document = Document.objects.filter(q, id=kwargs['document_id']).first()

        if not document:
            return Response({'errors': {'common': 'Документ не найден.'}}, status=status.HTTP_404_NOT_FOUND)

        StorageClient = storage.Client()
        bucket = StorageClient.get_bucket('edoc-cloud')
        f = bucket.blob('{}/{}'.format(document.id, document.name))
        data = f.download_as_string()

        content_type = f.content_type
        attachment = 'attachment;'

        if '.pdf' in document.name:
            content_type = 'application/pdf'
            attachment = ''

        response = HttpResponse(data, content_type=content_type)
        response['Content-Disposition'] = attachment+' filename="{}"'.format(quote(document.name))

        return response


class DocumentMoveToArchiveApi(generics.GenericAPIView,
                               SenderReceiverQuerysetMixin):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):

        q = self.get_receiver_queryset() | self.get_sender_queryset()

        document = Document.objects.filter(q, id=kwargs['document_id']).first()

        if not document:
            return Response({'errors': {
                'common': 'Документ не найден или находится в статусе недопустимом для переноса в архив.'}},
                status=status.HTTP_404_NOT_FOUND)

        if document.sender_phone == request.user.phone:
            document.sender_is_archived = True
            document.sender_archived_at = timezone.now()

        for certificate in self.certificates:
            if certificate.edrpou == document.receiver_edrpou \
                or certificate.drfo == document.receiver_drfo:
                document.receiver_is_archived = True
                document.receiver_archived_at = timezone.now()
                break

        document.save()

        return Response(
            DocumentSerializer(document, context=self.get_serializer_context()).data,
            status=status.HTTP_200_OK
        )


class DocumentShareApi(generics.GenericAPIView,
                       SenderReceiverQuerysetMixin):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):

        q = self.get_sender_queryset()

        document = Document.objects.filter(q, id=kwargs['document_id']).first()

        if not document:
            return Response({'errors': {
                'common': 'Документ не найден.'}},
                status=status.HTTP_404_NOT_FOUND)

        document.is_shared = True
        document.save()

        return Response(
            DocumentSerializer(document, context=self.get_serializer_context()).data,
            status=status.HTTP_200_OK
        )


class DocumentUnshareApi(generics.GenericAPIView,
                         SenderReceiverQuerysetMixin):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):

        q = self.get_sender_queryset()

        document = Document.objects.filter(q, id=kwargs['document_id']).first()

        if not document:
            return Response({'errors': {
                'common': 'Документ не найден.'}},
                status=status.HTTP_404_NOT_FOUND)

        document.is_shared = False
        document.save()

        return Response(
            DocumentSerializer(document, context=self.get_serializer_context()).data,
            status=status.HTTP_200_OK
        )
