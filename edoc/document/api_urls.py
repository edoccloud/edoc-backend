from django.urls import path, re_path
from edoc.document.api_views import DocumentDetailApi
from edoc.document.api_views import DocumentGetZipArchiveApi
from edoc.document.api_views import DocumentOriginalApi
from edoc.document.api_views import DocumentViewApi
from edoc.document.api_views import DocumentListApi
from edoc.document.api_views import DocumentSignApi
from edoc.document.api_views import DocumentDownloadSignApi
from edoc.document.api_views import DocumentSendApi
from edoc.document.api_views import DocumentMoveToArchiveApi
from edoc.document.api_views import DocumentShareApi
from edoc.document.api_views import DocumentUnshareApi


urlpatterns = [
    path('', DocumentListApi.as_view()),
    path('<str:document_id>/archive', DocumentMoveToArchiveApi.as_view()),
    re_path(r'(?P<mode>all|pending|inbox|sent|archive)$', DocumentListApi.as_view()),
    path('<str:document_id>/sign', DocumentSignApi.as_view()),
    path('<str:document_id>/sign/<path:drfo_edrpou>', DocumentDownloadSignApi.as_view(), name='sign_get_api'),
    path('<str:document_id>/send', DocumentSendApi.as_view()),
    path('<str:document_id>/archive/get', DocumentGetZipArchiveApi.as_view(), name='archive_get_api'),
    path('<str:document_id>/original/get', DocumentOriginalApi.as_view(), name='original_get_api'),
    path('<str:document_id>/view/get', DocumentViewApi.as_view(), name='view_get_api'),
    path('<str:document_id>/share', DocumentShareApi.as_view()),
    path('<str:document_id>/unshare', DocumentUnshareApi.as_view()),
    path('<str:pk>', DocumentDetailApi.as_view())
]
